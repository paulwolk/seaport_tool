import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import main.Database;
import main.Ship;




public class MainWindow
{
	private JFrame _Window = null;
	
	private JButton _BTN_Login = null;
	private JButton _BTN_AddShip = null;
	private JButton _BTN_EditShip = null;
	private JButton _BTN_DeleteShip = null;
	private JButton _BTN_CalculateTimeCritical = null;
	private JButton _BTN_CalculateResourceCritical = null;
	
	private JLabel _LBL_Fleet = null;
	private JLabel _LBL_AddShip = null;
	private JLabel _LBL_ShipName = null;
	private JLabel _LBL_ShipCapacity = null;
	private JLabel _LBL_ShipCrew = null;
	private JLabel _LBL_QuestData = null;
	private JLabel _LBL_QuestCrew = null;
	private JLabel _LBL_QuestResources = null;
	private JLabel _LBL_QuestMultiplier = null;

	
	private JList _LST_Ships = null;
	
	private JTextField _TXB_ShipName = null;
	private JTextField _TXB_ShipCapacity = null;
	private JTextField _TXB_ShipCrew = null;
	private JTextField _TXB_QuestCrew = null;
	private JTextField _TXB_QuestResources = null;
	private JTextField _TXB_QuestMultiplier = null;
	
	private Ship[] _Ships = null;
	
	public MainWindow()
	{
		createWindow();
		createControls();
	}
	
	public void Run()
	{		
		if(!Database.Connect("127.0.0.1", 3306, "seaport", "root", ""))
		{
			JOptionPane.showMessageDialog(null, "Verbindung zur Datenbank nicht m�glich.", "Verbindungsfehler", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		updateShipList();

		_Window.setVisible(true);
		
	}
	
	private void createWindow()
	{
		_Window = new JFrame("Seaport Tool");
		_Window.setSize(700, 740);
		_Window.setLocationRelativeTo(null);
		_Window.setLayout(null);
		_Window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_Window.setResizable(false);
	}
	
	private void createControls()
	{
		_BTN_Login = new JButton("Login");
		_BTN_Login.setSize(100, 30);
		_BTN_Login.setLocation(10, 10);
		/*_BTN_Login.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				JOptionPane.showMessageDialog(null, "kein icon", "titel", JOptionPane.PLAIN_MESSAGE);
			}
		});*/
		
		//_Window.add(_BTN_Login);
		
		_BTN_AddShip = new JButton("Schiff hinzuf�gen");
		_BTN_AddShip.setSize(150, 25);
		_BTN_AddShip.setLocation(450, 275);
		
		_BTN_AddShip.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(!validateShipValues())
				{
					JOptionPane.showMessageDialog(null, "Ung�ltige Schiffsdaten.", "Daten ung�ltig", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				LocalDateTime now = LocalDateTime.now();

				String uid = "";
				uid += Integer.toString(now.getYear());
				uid += Integer.toString(now.getMonthValue());
				uid += Integer.toString(now.getDayOfMonth());
				uid += Integer.toString(now.getHour());
				uid += Integer.toString(now.getMinute());
				uid += Integer.toString(now.getSecond());
				uid += Integer.toString(now.getNano());
				
				Ship ship = new Ship(_TXB_ShipName.getText(), Integer.parseInt(_TXB_ShipCapacity.getText()), Integer.parseInt(_TXB_ShipCrew.getText()), uid);
				if(Database.AddShip(1 /*User ID*/, ship.getName(), ship.getCrew(), ship.getCapacity(), ship.GetUID()))
				{
					updateShipList();
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Schiff konnte nicht zur Datenbank hinzugef�gt werden.", "Datenbankfehler", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		_Window.add(_BTN_AddShip);
		
		_BTN_EditShip = new JButton("Schiffsdaten �ndern");
		_BTN_EditShip.setSize(150, 25);
		_BTN_EditShip.setLocation(450, 310);
		
		_BTN_EditShip.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(!validateShipValues())
				{
					JOptionPane.showMessageDialog(null, "Ung�ltige Schiffsdaten.", "Daten ung�ltig", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				int index = _LST_Ships.getSelectedIndex();
				
				if(index >= 0)
				{				
					_Ships[index].setName(_TXB_ShipName.getText());
					_Ships[index].setCapacity(Integer.parseInt(_TXB_ShipCapacity.getText()));
					_Ships[index].setCrew(Integer.parseInt(_TXB_ShipCrew.getText()));
					
					if(Database.UpdateShip(1 /*User ID*/, _Ships[index].getName(), _Ships[index].getCrew(), _Ships[index].getCapacity(), _Ships[index].GetUID()))
					{
						updateShipList();
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Schiff konnte nicht editiert werden.", "Datenbankfehler", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		_Window.add(_BTN_EditShip);
		
		_BTN_DeleteShip = new JButton("Schiff l�schen");
		_BTN_DeleteShip.setSize(150, 25);
		_BTN_DeleteShip.setLocation(450, 345);
		
		_BTN_DeleteShip.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				int index = _LST_Ships.getSelectedIndex();
				
				if(index >= 0)
				{									
					if(Database.DeleteShip(_Ships[index].GetUID()))
					{
						updateShipList();
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Schiff konnte nicht gel�scht werden.", "Datenbankfehler", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		_Window.add(_BTN_DeleteShip);
		
		_BTN_CalculateTimeCritical = new JButton("<html>Zeitkritisch<br>berechnen</html>");
		_BTN_CalculateTimeCritical.setSize(200, 40);
		_BTN_CalculateTimeCritical.setLocation(100, 550);
		
		_BTN_CalculateTimeCritical.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(!validateQuestValues())
				{
					JOptionPane.showMessageDialog(null, "Ung�ltige Questdaten.", "Daten ung�ltig", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				JOptionPane.showMessageDialog(null, Algorithm.CalculateTimeCritical(_Ships, Integer.parseInt(_TXB_QuestCrew.getText()), Integer.parseInt(_TXB_QuestResources.getText()), Integer.parseInt(_TXB_QuestMultiplier.getText())), "Quest zeitkritisch berechnen", JOptionPane.PLAIN_MESSAGE);					
			}
		});
		
		_Window.add(_BTN_CalculateTimeCritical);
		
		_BTN_CalculateResourceCritical = new JButton("<html>Ressourcenkritisch<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;berechnen</html>");
		_BTN_CalculateResourceCritical.setSize(200, 40);
		_BTN_CalculateResourceCritical.setLocation(100, 600);
		
		_BTN_CalculateResourceCritical.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(!validateQuestValues())
				{
					JOptionPane.showMessageDialog(null, "Ung�ltige Questdaten.", "Daten ung�ltig", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				JOptionPane.showMessageDialog(null, Algorithm.CalculateResourceCritical(_Ships, Integer.parseInt(_TXB_QuestCrew.getText()), Integer.parseInt(_TXB_QuestResources.getText()), Integer.parseInt(_TXB_QuestMultiplier.getText())), "Quest zeitkritisch berechnen", JOptionPane.PLAIN_MESSAGE);					
			}
		});
		
		_Window.add(_BTN_CalculateResourceCritical);
		
		_LBL_Fleet = new JLabel("Flotte");
		_LBL_Fleet.setSize(100, 30);
		_LBL_Fleet.setLocation(100, 80);
		_LBL_Fleet.setFont(new Font("Impact", 0, 30));
		_Window.add(_LBL_Fleet);
		
		_LBL_AddShip = new JLabel("Schiff hinzuf�gen:");
		_LBL_AddShip.setSize(110, 30);
		_LBL_AddShip.setLocation(350, 120);
		_Window.add(_LBL_AddShip);
		
		_LBL_ShipName = new JLabel("Name");
		_LBL_ShipName.setSize(100, 30);
		_LBL_ShipName.setLocation(350, 160);
		_Window.add(_LBL_ShipName);
		
		_LBL_ShipCapacity = new JLabel("Kapazit�t");
		_LBL_ShipCapacity.setSize(100, 30);
		_LBL_ShipCapacity.setLocation(350, 190);
		_Window.add(_LBL_ShipCapacity);
		
		_LBL_ShipCrew = new JLabel("Crew");
		_LBL_ShipCrew.setSize(100, 30);
		_LBL_ShipCrew.setLocation(350, 220);
		_Window.add(_LBL_ShipCrew);
		
		_LBL_QuestData = new JLabel("Questdaten:");
		_LBL_QuestData.setSize(110, 30);
		_LBL_QuestData.setLocation(350, 400);
		_Window.add(_LBL_QuestData);
		
		_LBL_QuestCrew = new JLabel("Crew");
		_LBL_QuestCrew.setSize(110, 30);
		_LBL_QuestCrew.setLocation(350, 440);
		_Window.add(_LBL_QuestCrew);
		
		_LBL_QuestResources = new JLabel("Ressourcen");
		_LBL_QuestResources.setSize(110, 30);
		_LBL_QuestResources.setLocation(350, 470);
		_Window.add(_LBL_QuestResources);
		
		_LBL_QuestMultiplier = new JLabel("Multiplikator");
		_LBL_QuestMultiplier.setSize(110, 30);
		_LBL_QuestMultiplier.setLocation(350, 500);
		_Window.add(_LBL_QuestMultiplier);
		
				
		_LST_Ships = new JList();
		_LST_Ships.setSize(200, 410);
		_LST_Ships.setLocation(100, 120);
		_LST_Ships.setBackground(new Color(210, 210, 210));
		_LST_Ships.setVisibleRowCount(-1);
		_LST_Ships.setLayoutOrientation(JList.VERTICAL);

		_LST_Ships.addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent e) 
			{
				int index = _LST_Ships.getSelectedIndex();
				
				if(index >= 0)
				{				
					_TXB_ShipName.setText(_Ships[index].getName());
					_TXB_ShipCapacity.setText(Integer.toString(_Ships[index].getCapacity()));
					_TXB_ShipCrew.setText(Integer.toString(_Ships[index].getCrew()));
				}
			}
		});
		
		_Window.add(_LST_Ships);
		

		_TXB_ShipName = new JTextField();
		_TXB_ShipName.setSize(150, 20);
		_TXB_ShipName.setLocation(450, 165);
		_Window.add(_TXB_ShipName);
		
		_TXB_ShipCapacity = new JTextField();
		_TXB_ShipCapacity.setSize(150, 20);
		_TXB_ShipCapacity.setLocation(450, 195);
		_Window.add(_TXB_ShipCapacity);
		
		_TXB_ShipCrew = new JTextField();
		_TXB_ShipCrew.setSize(150, 20);
		_TXB_ShipCrew.setLocation(450, 225);
		_Window.add(_TXB_ShipCrew);
		
		_TXB_QuestCrew = new JTextField();
		_TXB_QuestCrew.setSize(150, 20);
		_TXB_QuestCrew.setLocation(450, 445);
		_Window.add(_TXB_QuestCrew);
		
		_TXB_QuestResources = new JTextField();
		_TXB_QuestResources.setSize(150, 20);
		_TXB_QuestResources.setLocation(450, 475);
		_Window.add(_TXB_QuestResources);
		
		_TXB_QuestMultiplier = new JTextField();
		_TXB_QuestMultiplier.setSize(150, 20);
		_TXB_QuestMultiplier.setLocation(450, 505);
		_Window.add(_TXB_QuestMultiplier);
	}
	
	private void updateShipList()
	{		
		_Ships = Database.getShips(1 /*User ID*/);
		
		DefaultListModel model = new DefaultListModel();
		
		for(Ship ship : _Ships)
		{
			model.addElement(ship.getName());
		}
		
		_LST_Ships.setModel(model);
	}
	
	private boolean validateShipValues()
	{
		try
		{
			Integer.parseInt(_TXB_ShipCrew.getText());
			Integer.parseInt(_TXB_ShipCapacity.getText());
			
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}
	
	private boolean validateQuestValues()
	{
		try
		{
			Integer.parseInt(_TXB_QuestCrew.getText());
			Integer.parseInt(_TXB_QuestResources.getText());
			Integer.parseInt(_TXB_QuestMultiplier.getText());
			
			return true;
		}
		catch(Exception ex)
		{
			return false;
		}
	}
}
