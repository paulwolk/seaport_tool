package main;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import main.Database.CommandType;

public class Database {

	enum CommandType {
		UPDATE, QUERY
	}

	private static Connection _Connection = null;

	private static ResultSet _Results = null;

	// connects to a database
	//
	// Params:
	// - host : hostname of the database
	// - port : connection port
	// - dbName : name of the database
	// - user : username credential
	// - password : password credential
	public static boolean Connect(String host, int port, String dbName, String user, String password) {
		try {
			_Connection = DriverManager.getConnection(
					"jdbc:mysql://" + host + ":" + Integer.toString(port) + "/" + dbName, user, password);
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

		return true;
	}

	// send an SQL command to the connected database
	//
	// Params:
	// - command : SQL command string
	// - type : execution type

	public static void Disconnect()
	{
		try {
			_Connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static boolean SendCommand(String command, CommandType type) {
		try {
			_Results = null;

			Statement statement = _Connection.createStatement();

			switch (type) {
			case UPDATE:
				statement.executeUpdate(command);
				break;
			case QUERY:
				_Results = statement.executeQuery(command);
				break;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

		return true;
	}

	// returns the result of the last SQL query
	public static ResultSet GetResult() {
		return _Results;
	}

	public static boolean AddUser(String username, String password) {
		return SendCommand(
				"INSERT INTO users (name, password) VALUES (\"" + username + "\", \"" + hash(hash(password)) + "\")",
				CommandType.UPDATE);
	}

	public static boolean AddShip(int userID, String name, int crew, int capacity, String uid) {
		return SendCommand("INSERT INTO ships (ship_name, crew, capacity, fk_user_id, uid) VALUES (\"" + name + "\", "
				+ Integer.toString(crew) + ", " + Integer.toString(capacity) + ", " + Integer.toString(userID) + ", " + uid + ")",
				CommandType.UPDATE);
	}
	
	public static boolean UpdateShip(int userID, String name, int crew, int capacity, String uid) {
		return SendCommand("UPDATE `ships` SET `ship_name` = '" + name + "', `crew` = " + Integer.toString(crew) + ", `capacity` = " + Integer.toString(capacity) + " WHERE `uid` = '" + uid + "'", CommandType.UPDATE);
	}

	public static boolean DeleteShip(String uid) {
		return SendCommand("DELETE FROM ships WHERE `uid`= '" + uid + "'", CommandType.UPDATE);
	}

	public static int Login(String username, String password) {

		System.out.println(SendCommand("SELECT `user_id`,`password` FROM `users` WHERE `name`=\"" + username + "\"", CommandType.QUERY));
		try {
			while (_Results.next()) {
				int user_id = _Results.getInt("USER_ID");
				String password_hash = _Results.getString("password");

				if (hash(hash(password)).equals(password_hash)) {
					return user_id;
				}
			}
			return -1;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	public static int getUsergroup(int user_id) {

		SendCommand("SELECT `user_group` FROM `users` WHERE `user_id`=" + user_id , CommandType.QUERY);
		try {
			while (_Results.next()) {
				return _Results.getInt("USER_GROUP");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	public static Ship[] getShips(int user_id) {

		SendCommand("SELECT COUNT(*) as c FROM `ships` WHERE `fk_user_id`=" + user_id, CommandType.QUERY);
		
		int countShips = -1;
		
		try {
			while (_Results.next()) {
				countShips = _Results.getInt("c");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		
		SendCommand("SELECT `ship_name`,`crew`,`capacity`, `uid` FROM `ships` WHERE `fk_user_id`=" + user_id, CommandType.QUERY);
		
		Ship[] ships = new Ship[countShips];
		int iter = 0;
		
		try {
			while (_Results.next()) {
				ships[iter] = new Ship(_Results.getString("ship_name"),_Results.getInt("capacity"), _Results.getInt("crew"), _Results.getString("uid"));
				iter++;
			}
			return ships;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static String hash(String data) {
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("SHA-512");
			messageDigest.update(data.getBytes());
			byte[] messageDigestMD5 = messageDigest.digest();
			StringBuffer stringBuffer = new StringBuffer();
			for (byte bytes : messageDigestMD5) {
				stringBuffer.append(String.format("%02x", bytes & 0xff));
			}
			return stringBuffer.toString();
		} catch (NoSuchAlgorithmException exception) {
			// TODO Auto-generated catch block
			exception.printStackTrace();
		}
		return null;
	}
	
	public static boolean changePassword (int user_id, String newpassword) {
		
		String command = "UPDATE users SET password='"+hash(hash(newpassword))+"' WHERE user_id="+user_id;
		return SendCommand(command, CommandType.UPDATE);
		
	}
}
