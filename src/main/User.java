package main;

import java.sql.SQLException;

import main.Database.CommandType;

public class User {

	private Ship[] ships;
	private int userid;
	private String username;
	private int user_group;
	private boolean isLoggedIn = false;
	
	public User() {
		
		
	}
	
	public boolean login(String username, String password) {
		this.userid = Database.Login(username, password);
		if (this.userid != -1) {
			this.isLoggedIn = true;
			this.username = username;
			this.user_group = Database.getUsergroup(this.userid);
			this.ships = Database.getShips(this.userid);
		} else {
			return false;
		}
		return true;
	}

	public Ship[] getShips() {
		return ships;
	}

	public int getUserid() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public int getUser_group() {
		return user_group;
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}
	
	
	
}
