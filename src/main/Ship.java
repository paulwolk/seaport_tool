package main;

public class Ship {

	private String _Name = "";
	private int _Capacity = 0;
	private int _Crew = 0;
	
	private String _UID = "";
	
	public Ship( String name, int capacity, int crew, String uid) {
		_Name = name;
		_Capacity = capacity;
		_Crew = crew;
		
		_UID = uid;
	}

	public String getName() {
		return _Name;
	}

	public void setName(String name) {
		this._Name = name;
	}

	public int getCapacity() {
		return _Capacity;
	}

	public void setCapacity(int capacity) {
		this._Capacity = capacity;
	}

	public int getCrew() {
		return _Crew;
	}

	public void setCrew(int crew) {
		this._Crew = crew;
	}
	
	public String GetUID()
	{
		return _UID;
	}

}
