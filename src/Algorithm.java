import java.util.ArrayList;

import main.Ship;

public class Algorithm {

	// Zeitkritischer Algorithmus
	public static String CalculateTimeCritical(Ship[] ships, int questCrew, int questResources, int questMultiplier)
	{
		int targetCap = (int)(questResources / questMultiplier);
		int cap = 0;
		int crew = 0;
		
		Ship[] ordered = orderShipsByCapacity(ships);
		
		String result = "";

		result += "Quest L�sung:\n\n";
		
		for(int s = 0; s < ordered.length; s++)
		{
			cap += ordered[s].getCapacity();
			crew += ordered[s].getCrew();
			
			result += Integer.toString(s + 1) + ". - Schiff '" + ordered[s].getName() + "': Kapazit�t: " + Integer.toString(ordered[s].getCapacity()) + ", Crew: " + ordered[s].getCrew() + "\n";
		
			if(cap >= targetCap && crew >= questCrew)
			{
				break;
			}
		}
		
		if(cap < targetCap)
		{
			result = "Die Ressourcen der Quest k�nnen mit der aktuellen Flotte nicht erf�llt werden.";
		}
		
		if(crew < questCrew)
		{
			result = "Die Crew, die f�r diese Quest ben�tigt wird, ist zu gro�.";
		}
		
		return result;
	}
	
	// Ressourceskritischer Algorithmus
	public static String CalculateResourceCritical(Ship[] ships, int questCrew, int questResources, int questMultiplier)
	{
		int targetCap = (int)(questResources / questMultiplier);
		int cap = 0;
		int crew = 0;
		
		int bestOverhang = Integer.MAX_VALUE;
		ArrayList<Ship> bestPerm = null;	
		
		ArrayList<String> forbidden = new ArrayList<String>();
		ArrayList<ArrayList<Ship>> perm = new ArrayList<ArrayList<Ship>>();
		
		for(int i = 1; i < ships.length; i++)
		{
			ArrayList<ArrayList<Ship>> ps = permute(ships, forbidden, 1, i);
			
			for(int p = 0; p < ps.size(); p++)
			{
				perm.add(ps.get(p));
			}
		}		
		
		for(int p = 0; p < perm.size(); p++)
		{
			cap = accumulateCapacity(perm.get(p));
			crew = accumulateCrew(perm.get(p));
			
			if(cap >= targetCap)
			{
				int overhang = cap - targetCap;
				
				if(overhang <= bestOverhang)
				{
					bestPerm = perm.get(p);
					bestOverhang = overhang;
				}
			}
		}
		
		String result = "";
		
		if(bestPerm == null)
		{
			result = "Die Quest kann mit der aktuellen Flotte nicht erf�llt werden.";
		}
		else
		{
			for(int s = 0; s < bestPerm.size(); s++)
			{				
				result += Integer.toString(s + 1) + ". - Schiff '" + bestPerm.get(s).getName() + "': Kapazit�t: " + Integer.toString(bestPerm.get(s).getCapacity()) + ", Crew: " + bestPerm.get(s).getCrew() + "\n";
			}
			
			result += "\nUnbenutze Kapazit�t: " + Integer.toString(bestOverhang);
		}
		
		if(cap < targetCap)
		{
			result = "Die Ressourcen der Quest k�nnen mit der aktuellen Flotte nicht erf�llt werden.";
		}
		
		if(crew < questCrew)
		{
			result = "Die Crew, die f�r diese Quest ben�tigt wird, ist zu gro�.";
		}
		
		return result;
	}
	
	private static ArrayList<ArrayList<Ship>> permute(Ship[] ships, ArrayList<String> forbidden, int gen, int maxGen)
	{
		ArrayList<ArrayList<Ship>> perms = new ArrayList<ArrayList<Ship>>();
		
		if(gen < maxGen)
		{
			for(int i = 0; i < ships.length; i++)
			{
				if(!forbidden.contains(ships[i].GetUID()))
				{
					ArrayList<String> forbid = new ArrayList<String>(forbidden);
					forbid.add(ships[i].GetUID());
					
					ArrayList<ArrayList<Ship>> ps = permute(ships, forbid, gen + 1, maxGen);
					
					for(ArrayList<Ship> p : ps)
					{
						ArrayList<Ship> current = new ArrayList<Ship>();
						current.add(ships[i]);
						
						for(int v = 0; v < p.size(); v++)
						{
							current.add(p.get(v));
						}
						
						perms.add(current);
					}
				}
			}
		}
		else
		{
			for(int i = 0; i < ships.length; i++)
			{
				if(!forbidden.contains(ships[i].GetUID()))
				{
					ArrayList<Ship> num = new ArrayList<Ship>();
					num.add(ships[i]);
					perms.add(num);
				}
			}
		}
		
		return perms;
	}
	
	private static int accumulateCrew(ArrayList<Ship> ships)
	{
		int crew = 0;
		
		for(int i = 0; i < ships.size(); i++)
		{
			crew += ships.get(i).getCrew();
		}
		
		return crew;
	}
	
	private static int accumulateCapacity(ArrayList<Ship> ships)
	{
		int capacity = 0;
		
		for(int i = 0; i < ships.size(); i++)
		{
			capacity += ships.get(i).getCapacity();
		}
		
		return capacity;
	}
	
	private static Ship[] orderShipsByCapacity(Ship[] ships)
	{
		for(int i = 1; i < ships.length; i++)
		{
			for(int j = 0; j < ships.length - 1; j++)
			{
				if(ships[j].getCapacity() < ships[j + 1].getCapacity())
				{
					Ship tmp = ships[j + 1];
					ships[j + 1] = ships[j];
					ships[j] = tmp;
				}
			}
		}
		
		return ships;
	}
}
